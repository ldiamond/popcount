#include "hammingwLookup.h" 
#include <smmintrin.h>
#include <iostream>
#include <ctime>

static const unsigned int MAX = 65536;
static const unsigned int LOOPS = 100000;

static constexpr auto a = hammingw<16>(); 

typedef unsigned (*fn)(unsigned);

inline unsigned builtin64(unsigned i)
{
    return __builtin_popcountll(i);           
}
 
inline unsigned builtin32(unsigned i)
{
    return __builtin_popcount(i);           
}
 
inline unsigned swar(unsigned i)
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;           
}
 
inline unsigned lookup(unsigned i){
    return a[i];
}

void report(clock_t start, clock_t stop, unsigned long total, std::string fn)
{
    std::cout << fn << std::endl;
    std::cout << "Clock: " << stop-start << std::endl;
    std::cout << "Time: " << (stop-start-0.0f)/CLOCKS_PER_SEC << std::endl;
    std::cout << "Total: " << total << std::endl;
    std::cout << "------" << std::endl;
 

}

int main()
{
    unsigned long x;
    clock_t start;
    clock_t stop;
    
    x=0;
    start = clock();
    for (int i{}; i < LOOPS; ++i)
        for(int j{}; j < MAX; ++j)
            x+=builtin32(j);
    stop = clock();
    report(start, stop, x, "Builtin32"); 
    
    x=0;
    start = clock();
    for (int i{}; i < LOOPS; ++i)
        for(int j{}; j < MAX; ++j)
            x+=builtin64(j);
    stop = clock();
    report(start, stop, x, "Builtin64"); 
    
    x=0;
    start = clock();
    for (int i{}; i < LOOPS; ++i)
        for(int j{}; j < MAX; ++j)
            x+=lookup(j);
    stop = clock();
    report(start, stop, x,"Lookup"); 
    
    x=0;
    start = clock();
    for (int i{}; i < LOOPS; ++i)
        for(int j{}; j < MAX; ++j)
            x+=swar(j);
    stop = clock();
    report(start, stop, x, "swar"); 
  
}






