#include <array>
#include <cmath>
#include <iostream>
#include <ctime>
#include <vector>

template <class T>
constexpr T inc(T n) {
  return n+1;
}

template <unsigned N, std::size_t S, class T, T ...values>
constexpr
typename std::enable_if<N==0,std::array<T, S>>::type
hammingw() 
{
   return std::array<T,S>{{values...}};
}

template <unsigned N, std::size_t S, class T, T ...values>
constexpr
typename std::enable_if<(N > 0), std::array<T,S>>::type 
hammingw() 
{
   return hammingw<N-1, S, T, values..., inc(values)...>();  
}

template <unsigned N, class T = unsigned char>
constexpr
std::array<T, 1<<N> hammingw()
{
   return hammingw<N, 1<<N, T, T{}>();
}



