Intel Popcount vs Lookup Table
==============================

Overview
------------------------------
This is a quick benchmark of a lookup table based population count vs the built-in instruction in intel CPUs (called through GCC builtin). It is by no mean supposed to be exact; it was never peer reviewed and does not include analysis of the generated machine code. I made sure to remove indirections such as function pointers in the benchmark procedure in order to let the compiler optimize.

Using g++ 4.6.3: g++ -O3 -std=c++0x benchmark.cpp -msse4.2

The lookup table is generated using C++11 variadic templates. The next improvement is to remove the re-computation of already instanciated templates when using a higher number of bits (e.g. hammingw<14>, later hammingw<16>).

On my nehalem i7-920... Bultin wins.

Builtin32
Clock: 4550000
Time: 4.55
Total: 52428800000
------
Builtin64
Clock: 4530000
Time: 4.53
Total: 52428800000
------
Lookup
Clock: 6850000
Time: 6.85
Total: 52428800000
------
swar
Clock: 20070000
Time: 20.07
Total: 52428800000
------
